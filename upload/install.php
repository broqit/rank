<?php
/**
 * Rank
 * Last Updated: $Date: 2016-03-27T02:57:21+02:00 $
 *
 * @version 1.2.7
 * @author Roman Borkunov [icq: 420159], [skype: coderlaba.com], [email: ya.qnut@ya.ru]
 * @package install.php
 * @license http://my.bqust.com/license [Rank]
 * @copyright (c) 2014 - 2016
 * @link http://my.bqust.com
 *
 */

@error_reporting ( E_ALL ^ E_WARNING ^ E_NOTICE );
@ini_set ( 'display_errors', true );
@ini_set ( 'html_errors', false );
@ini_set ( 'error_reporting', E_ALL ^ E_WARNING ^ E_NOTICE );

define( 'DATALIFEENGINE', true );
define( 'ROOT_DIR', '.' );
define( 'ENGINE_DIR', ROOT_DIR . '/engine' );

include ENGINE_DIR . '/data/config.php';
require_once ENGINE_DIR . '/classes/mysql.php';
require_once ENGINE_DIR . '/data/dbconfig.php';

$isUTF = ( strtolower( $config['charset'] ) == 'utf-8' ) ? true : false;
$charset = $isUTF ? 'utf8' : 'cp1251';
$filec = explode( DIRECTORY_SEPARATOR, __FILE__ );
$thisFile = end($filec);

$db_charset = "{$config['charset']}";
$db_collate = "{$config['charset']}_general_ci";
$action = ( isset( $_REQUEST['action'] ) ) ? $_REQUEST['action'] : "main";


function convert_page($text, $charset = false) {
  return ($charset) ? iconv("WINDOWS-1251", "UTF-8", $text) : $text;
}
function echoheader($title) {
  global $config;
  echo <<<HTML
  <!DOCTYPE HTML>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset={$config['charset']}" />
  <title>{$title}</title>
  <style>html,body{font:13px/18px Arial,Helvetica,sans-serif;color:#333;background:#475F70;margin:0;padding:0}code,pre{font-family:Menlo,Monaco,Consolas,"Courier New",monospace}code{padding:2px 4px;font-size:90%;color:#c7254e;background-color:#f9f2f4;white-space:nowrap;border-radius:4px}pre,ol li textarea{display:block;padding:10px;margin:5px 0 10.5px;font-size:14px;line-height:1.42857143;word-break:break-all;word-wrap:break-word;color:#303030;background-color:#ebebeb;border:1px solid #ccc}pre code{padding:0;font-size:inherit;color:inherit;white-space:pre-wrap;background-color:transparent;border-radius:0}.pre-scrollable{max-height:340px;overflow-y:scroll}.label{display:inline;padding:.2em .6em .3em;font-size:75%;line-height:1;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;border-radius:.25em}.label[href]:hover,.label[href]:focus{color:#fff;text-decoration:none;cursor:pointer}.label:empty{display:none}.btn .label{position:relative;top:-1px}.label-default{background-color:#464545}.label-default[href]:hover,.label-default[href]:focus{background-color:#2c2c2c}.label-primary{background-color:#375a7f}.label-primary[href]:hover,.label-primary[href]:focus{background-color:#28415b}.label-success{background-color:#00bc8c}.label-success[href]:hover,.label-success[href]:focus{background-color:#008966}.label-info{background-color:#238DB1}.label-info[href]:hover,.label-info[href]:focus{background-color:#217dbb}.label-warning{background-color:#f39c12}.label-warning[href]:hover,.label-warning[href]:focus{background-color:#c87f0a}.label-danger{background-color:#e74c3c}.label-danger[href]:hover,.label-danger[href]:focus{background-color:#d62c1a}.wrapper{width:800px;overflow:hidden;margin:2% auto;-webkit-box-shadow:rgba(0,0,0,0.3) 0 1px 3px;-moz-box-shadow:rgba(0,0,0,0.3) 0 1px 3px;box-shadow:rgba(0,0,0,0.3) 0 1px 3px;border-color:#E5E5E5 #DBDBDB #D2D2D2;background:#FFF;position:relative}.wrapper .cms{position:absolute;top:0;font-size:18px;right:0;padding:5px 10px;color:#fff;background-color:#6A8A4B;border-radius:0 0 0 3px}.wrapper .date{position:absolute;top:0;font-size:13px;left:0;padding:5px 10px;font-style:italic;color:#fff;border-radius:0 0 3px 0;background-color:#6A8A4B}.wrapper .result{margin-top:10px}.wrapper .of_page{margin:15px 0;text-align:center}.wrapper .of_page a{text-decoration:none;color:#787878;background:#D77E47;color:#fff;border-radius:3px;padding:5px 10px}ol li{margin:0;padding:10px 0;border-bottom:1px solid #ebebeb}ol li textarea{width:100%;background-color:#ebebeb;outline:none;border:1px solid #ccc;height:auto;padding:5px}header{background:#334E61;padding:15px}header .title{color:#fff;text-align:center}header .title sup,header .title sub{display:block}header .title sup{font-size:28pt;font-weight:700;line-height:40pt}header .title sub{font-size:14px}section{padding:30px}footer{background:#E5E5E5;padding:5px 15px}table{max-width:100%;background-color:transparent;border-spacing:0;width:100%;margin-bottom:20px;border:1px solid #ddd;border-collapse:separate;*border-collapse:collapse;border-left:0}table th,table td{padding:8px;line-height:20px;vertical-align:top;border-top:1px solid #ddd;border-left:1px solid #ddd}table td a{color:#4C6589}table th{font-weight:700;color:#4C6589}table thead th{vertical-align:bottom}table caption + thead tr:first-child th,table caption + thead tr:first-child td,table colgroup + thead tr:first-child th,table colgroup + thead tr:first-child td,table thead:first-child tr:first-child th,table thead:first-child tr:first-child td{border-top:0}table caption + thead tr:first-child th,table caption + tbody tr:first-child th,table caption + tbody tr:first-child td,table colgroup + thead tr:first-child th,table colgroup + tbody tr:first-child th,table colgroup + tbody tr:first-child td,table thead:first-child tr:first-child th,table tbody:first-child tr:first-child th,table tbody:first-child tr:first-child td{border-top:0}table tbody tr:hover > td,table tbody tr:hover > th{background-color:#f5f5f5}table tbody tr.success > td{background-color:#dff0d8}table tbody tr.error > td{background-color:#f2dede}table tbody tr.warning > td{background-color:#fcf8e3}table tbody tr.info > td{background-color:#d9edf7}
  .wrapper .result { margin-top: 10px; text-align: center; font-size: 18px;}
  .wrapper .descr { margin-top: 10px; text-align: center; font-size: 14px;}</style>
</head>
<body>
HTML;
}
function echofooter() {
  echo '</body>
</html>';
}
function chmods($data) {
  return substr(decoct( fileperms($data) ), 3);
}
function charset_conv ( $val ) {
  global $config;
  return ($config['charset'] != 'utf-8') ? iconv($config['charset'], "WINDOWS-1251", $val) : iconv("WINDOWS-1251", $config['charset'], $val);
}
$install_config = array(
  'prefix' => 'rank',
  'module' => 'rank',
  'version' => '1.2.7',
  'dle_ver' => '10.2 - 11.0',
  'date' => '27.03.2016',
  'language' => array(
    '��������� ������ ',
    '������ / Rank',
    '��������� ������',
    '����������',
    '���������� ��������',
    '���������',
    '���������',
    '�������',
    '����������',
    '������� ����� ������������',
    '�������� ���� CHMOD',
    '��',
    '��������� ������� ���������.',
    '�� �������� ������� ���� ���������',
    '��������� ����������.',
    '����������� ����',
    '��� �� ����������� �����',
  ),
   'installed' => array(
    'title' => charset_conv ('��������� ������� ���������.'),
    'descr' => charset_conv ('�� �������� ������� ���� ���������').' <code>' . $thisFile . '</code>.',
  ),
  'notinstalled' => array(
    'title' => charset_conv ($install_config['language'][14]),
    'descr' => charset_conv ($install_config['language'][15]).' <code>engine/data/'.$install_config['prefix'].'_config.php</code> '.charset_conv ($install_config['language'][16]).' <span class="label label-info">CHMOD</span>',
  ),
);

$install_config['language'] = array_map("charset_conv", $install_config['language']);

$file = ENGINE_DIR . '/data/'.$install_config['prefix'].'_config.php';
$file_e = @file_exists($file) ? '<span class="label label-success">'.$install_config['language'][5].'</span>' : '<span class="label label-danger">'.$install_config['language'][6].'</span>';

if (chmods($file) == '666') {
  $buttons = '<div class="of_page"><a href="/install.php?action=installed" style="background:#6A8A4B">'.$install_config['language'][3] . '</a></div>';
  $chmod = '<span class="label label-success">'.$install_config['language'][5].'</span>';
} else  $chmod = '<span class="label label-danger">'.$install_config['language'][6].'</span>';

echoheader($install_config['language'][0].' '.$install_config['language'][1]);

if ($action == "main") {
$page = '<table><thead>
          <tr>
            <th>'.$install_config['language'][7] . '</th>
            <th style="text-align:center">'.$install_config['language'][8] . '</th>
          </tr>
        </thead>
        <tr>
        <td>'.$install_config['language'][9] . ' <code>engine/data/'.$install_config['prefix'].'_config.php</code></td>
        <td style="text-align:center">' . $file_e .'</td>
        </tr>
        <tr>
        <td>'.$install_config['language'][10] . ' <code>engine/data/'.$install_config['prefix'].'_config.php</code> - '.chmods($file).'</td>
        <td style="text-align:center">' . $chmod .'</td>
        </tr>
      </table>'.$buttons;
} elseif ($action == "installed" AND chmods($file) == '666') {
  $dataBase = array( );
  $dataBase[] = "CREATE TABLE IF NOT EXISTS `{PREFIX}_rank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `descr` text NOT NULL,
  `count` mediumint(8) NOT NULL DEFAULT '0',
  `user_id` varchar(100) NOT NULL DEFAULT '',
  `banned_id` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET={CHAR} AUTO_INCREMENT=1;";
  //TYPE=MyISAM DEFAULT CHARSET={CHAR} AUTO_INCREMENT=1


  $dataBase[] = 'INSERT INTO `{PREFIX}_rank`(`id`, `name`, `descr`, `count`) VALUES
(1, "����������", "��������� ������", "0"),
(2, "�������", "�� ���������� 5 �������", "5"),
(3, "��������", "�� ���������� 45 �������", "45"),
(4, "��. �������", "�� ���������� 163 �������", "163"),
(5, "�������", "�� ���������� 235 �������", "235"),
(6, "��. �������", "�� ���������� 454 �������", "454"),
(7, " ��������", "�� ���������� 892 �������", "892"),
(8, "���������", "�� ���������� 1111 �������", "1111"),
(9, "��. ���������", "�� ���������� 1330 �������", "1330"),
(10, "��. ���������", "�� ���������� 1549 �������", "1549"),
(11, "���������", "�� ���������� 1987 �������", "1987"),
(12, "��. ���������", "�� ���������� 2206 �������", "2206"),
(13, "�������", "�� ���������� 2644 �������", "2644"),
(14, "�����", "�� ���������� 2863 �������", "2863"),
(15, "������������", "�� ���������� 3082 �������", "3082"),
(16, "���������", "�� ���������� 3301 �������", "3301"),
(17, "�������", "�� ���������� 3739 �������", "3379"),
(18, "������", "�� ���������� 3958 �������", "398");';

  $dataBase[] = "INSERT INTO `{PREFIX}_admin_sections` (`name`, `title`, `descr`, `icon`, `allow_groups`) VALUES ('{$install_config['module']}', '{$install_config['language'][1]}', '{$install_config['language'][4]}', '{$install_config['module']}.png', '1')";

  if ( is_array( $dataBase ) and count( $dataBase ) > 0 ) foreach ( $dataBase as $dataQuery ) $db->query( str_replace( array( '{PREFIX}', '{CHAR}' ), array( PREFIX, $charset ), $dataQuery ) );

  $page = '<div class="result">' . $install_config[$action]['title'] . '</div><div class="descr">' . $install_config[$action]['descr'] . '</div>';
} else $page = '<div class="result">' . $install_config['notinstalled']['title'] . '</div><div class="descr">' . $install_config['notinstalled']['descr'] . '</div>';

echo '<div class="wrapper">
    <header>
      <div class="title">
        <sup>'.$install_config['language'][1] . ' ' . $install_config['version'] .'</sup>
        <sub>'.$install_config['language'][0].'</sub>
      </div>
      <div class="cms">DLE <b>' . $install_config['dle_ver'] .'</b></div>
      <div class="date">'.$install_config['language'][11] . ' <span>' . $install_config['date'] .'</span></div>
    </header>
    <section>' . $page .'</section>
    <footer>
      <div class="of_page"><a href="http://my.bqust.com/" target="_blank">'.$install_config['language'][2] . '</a></div>
    </footer>
  </div>';
echofooter();
?>