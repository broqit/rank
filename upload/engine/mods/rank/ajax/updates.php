﻿<?php
/**
 * Rank
 * Last Updated: $Date: 2016-10-08T20:28:50+03:00 $
 *
 * @version 1.3
 * @author Roman Borkunov [icq: 420159], [skype: coderlaba.com], [email: ya.qnut@ya.ru]
 * @package engine/mods/rank/ajax/updates.php
 * @license http://coderiz.pw/license [Rank]
 * @copyright (c) 2014 - 2016
 * @link http://coderiz.pw
 *
 */

@error_reporting( E_ALL ^ E_NOTICE );
@ini_set( 'display_errors', true );
@ini_set( 'html_errors', false );
@ini_set( 'error_reporting', E_ALL ^ E_NOTICE );

define( 'DATALIFEENGINE', true );
define( 'ROOT_DIR', substr( dirname(  __FILE__ ), 0, -21 ) );
define( 'ENGINE_DIR', ROOT_DIR . '/engine' );

include_once ENGINE_DIR . '/data/config.php';
include_once ROOT_DIR . 'language/'.$config['langs'].'/rank.lng';

function charset_conv ( $val ) {
    global $config;
    return ($config['charset'] != 'utf-8') ? iconv($config['charset'], "UTF-8", $val) : iconv("UTF-8", $config['charset'], $val);
}

$update = file_get_contents("http://coderiz.pw/updates.php?module_id={$_REQUEST['module_id']}&version_id={$_REQUEST['version_id']}&hash=".urlencode(base64_encode($_SERVER['HTTP_HOST'])));
$update = ( empty( $update ) ) ? "<div class=\"alert alert-danger\">".charset_conv($r_lang['txt_55'])."</div>" : "<div class=\"alert alert-success\">".charset_conv($r_lang['txt_56']). $update."</div>";
echo $update;
?>