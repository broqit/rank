<?php
# @Author: Roman Borkunov <broqit>
# @Date:   2017-12-05T04:30:07+02:00
# @Email:  ya.qnut@gmail.com
# @Project: Rank
# @Filename: rank.php
# @Last modified by:   broqit
# @Last modified time: 2017-12-05T04:45:29+02:00
# @License: https://coderiz.pw/license
# @Copyright: 2014 - 2017

if (!defined('DATALIFEENGINE')) {
    die("Hacking Attempt!");
}

require ENGINE_DIR . '/data/rank_config.php';

if( $rank_config['status'] ) {
    $section = ($rank_config['section_txt']) ? $rank_config['section_txt'] : $rank_config['section'];

    $r_num = $row[$section];

    $sql = "SELECT user_id FROM ".PREFIX."_rank WHERE ".$row['user_id']." IN (user_id)";
    $set = (isset($this)) ? $this->db->super_query($sql) : $db->super_query($sql);
    unset($sql);

    $where = (!$set['user_id']) ? " (count BETWEEN '0' AND '$r_num') or " : "";

    $sql = "SELECT descr, name, id, count as count_now, (SELECT count FROM ".PREFIX."_rank WHERE count > count_now ORDER BY count limit 0,1) as next_rank, (SELECT name FROM ".PREFIX."_rank WHERE count > count_now ORDER BY id limit 0,1) as next_name FROM ".PREFIX."_rank where{$where} (".$row['user_id']." IN (user_id) AND ".$row['user_id']." NOT IN (banned_id)) ORDER BY count desc";
    $rank = (isset($this)) ? $this->db->super_query($sql) : $db->super_query($sql);

    if (!function_exists('declOfNum')) {
        function declOfNum($number, $titles) {
            $cases = [2, 0, 1, 1, 1, 2];
            return $titles[ ($number%100 > 4 && $number %100 < 20) ? 2 : $cases[min($number%10, 5)] ];
        }
    }
    switch ($section) {
        case 'comm_num':
            $array = ["комментарий","комментария","комментариев"];
            break;

        case 'news_num':
            $array = ["пост","поста","постов"];
            break;

        case 'points':
            $array = ["балл","балла","баллов"];
            break;

        default:
            $array = ["очко","очка","очков"];
            break;
    }

    $num = round($rank['next_rank'] - $r_num);

    $point = $num ." ". declOfNum($num, $array);

    $tpl->set_block( "'\\[rank\\](.*?)\\[/rank\\]'si", "\\1" );
    $tpl->set( '{rank_id}', $rank['id'] );
    $tpl->set( '{rank_descr}', $rank['descr'] );
    if($member_id['user_id'] == $row['user_id']) {
        $your = "Вы достигли";
        $your_2 = "Вам";
    } else {
        $your = "Пользователь достиг";
        $your_2 = "Пользователю";
    }
    $tpl->set( '{next_rank}', (preg_match("/-/i", $point)) ? $your . " предельного звания!" : "До следующего звания {$your_2} осталось " . $point);
    $tpl->set( '{next_name}', $rank['next_name'] );
    $tpl->set( '{rank}', ($user_group[$row['user_group']]['id'] == 1 ? $user_group[$row['user_group']]['group_name'] : $rank['name']) );
    $tpl->set( '{rank_img}', $config['http_home_url'] . "templates/" . $config['skin'] . "/rank/img/" . $rank['id'] . ".png" );
} else {
    $tpl->set_block( "'\\[rank\\](.*?)\\[/rank\\]'si", "" );
}
?>
