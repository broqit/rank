<?php
/**
 * Rank
 * Last Updated: $Date: 2016-10-08T22:04:12+03:00 $
 *
 * @version 1.3
 * @author Roman Borkunov [icq: 420159], [skype: coderlaba.com], [email: ya.qnut@ya.ru]
 * @package engine/mods/rank/backend/list.php
 * @license http://coderiz.pw/license [Rank]
 * @copyright (c) 2014 - 2016
 * @link http://coderiz.pw
 *
 */

if( !defined( 'DATALIFEENGINE' ) OR !defined( 'LOGGED_IN' ) ) die( "Hacking attempt!" );

$db->query("SELECT * FROM ".PREFIX."_rank ORDER BY id");
	if($db->num_rows()){
		$mod_list = <<<HTML
	<table class="table">
      <thead>
        <tr>
          <th>#</th>
          <th colspan="2">{$r_lang['txt_15']}</th>
          <th colspan="2">{$r_lang['txt_17']}</th>
          <th colspan="2">{$r_lang['txt_18']}</th>
          <th colspan="2">{$r_lang['txt_19']}</th>
          <th style="text-align:right">{$r_lang['txt_20']}</th>
        </tr>
      </thead>
      <tbody>
HTML;
		while($row = $db->get_row()){
			$row['user_id'] = ($row['user_id']) ? $row['user_id'] : $r_lang['txt_16'];
			$row['banned_id'] = ($row['banned_id']) ? $row['banned_id'] : $r_lang['txt_16'];
			$mod_list .= <<<HTML
			<tr>
          <td>{$row['id']}</td>
          <td colspan="2" title="{$row['descr']}">{$row['name']}</td>
          <td colspan="2">{$row['count']}</td>
          <td colspan="2">{$row['user_id']}</td>
          <td colspan="2">{$row['banned_id']}</td>
          <td style="text-align:right">
          	<a href="{$PHP_SELF}?mod=rank&action=editrank&rankid={$row['id']}" class="btn btn-xs btn-default">{$r_lang['txt_21']}</a>
          	<a onclick="javascript:cdelete('{$row[id]}'); return(false);" href="?mod=rank&action=removerank&rankid={$row['id']}" class="btn btn-xs btn-warning">x</a>
          </td>
        </tr>
HTML;
		}
		$mod_list.= <<<HTML
	 </tbody>
    </table>
HTML;
	} else $mod_list .= "<div style=\"text-align:center\">{$r_lang['txt_22']}</div>";
echo <<<HTML
<h1 class="page-header nopad">{$r_lang['txt_23']}</h1>
<div class="panel panel-default">
  <div class="panel-body">
        {$mod_list}
    </div>
    <div class="panel-footer">
    	<button class="btn btn-success btn-sm" data-toggle="modal" data-target="#RankListAdd">{$r_lang['txt_24']}</button>
    </div>
</div>
<script type=text/javascript>
<!--
function cdelete(id){
	DLEconfirm( '{$r_lang['txt_25']}', '{$lang['p_confirm']}', function () {
		document.location='?mod=rank&action=removerank&rankid=' + id + '';
	});
};
-->
</script>
<!-- Modal -->
<div class="modal fade" id="RankListAdd" tabindex="-1" role="dialog" aria-labelledby="RankListLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="RankListLabel">{$r_lang['txt_26']}</h4>
      </div>
      <div class="modal-body">
		<form action="" method="post">
			<div class="row box-section">
				<div class="input-group">
					<input type="text" name="name" class="form-control" placeholder="{$r_lang['txt_27']}">
					<span class="input-group-btn"><button class="btn btn-warning" type="button" data-rel="popover" data-trigger="hover" data-placement="right" data-content="{$r_lang['txt_28']}">?</button></span>
				</div><!-- /input-group -->
			</div>
			<div class="row box-section">
				<div class="input-group">
					<input type="text" name="descr" class="form-control" placeholder="{$r_lang['txt_29']}">
					<span class="input-group-btn"><button class="btn btn-warning" type="button" data-rel="popover" data-trigger="hover" data-placement="right" data-content="{$r_lang['txt_30']}">?</button></span>
				</div><!-- /input-group -->
			</div>
			<div class="row box-section">
				<div class="input-group">
					<input type="text" name="count" class="form-control" placeholder="{$r_lang['txt_31']}">
					<span class="input-group-btn"><button class="btn btn-warning" type="button" data-rel="popover" data-trigger="hover" data-placement="right" data-content="{$r_lang['txt_32']}">?</button></span>
				</div><!-- /input-group -->
			</div>
			<div class="row box-section">
				<div class="input-group">
					<input type="text" name="user_id" class="form-control" placeholder="{$r_lang['txt_33']}">
					<span class="input-group-btn"><button class="btn btn-warning" type="button" data-rel="popover" data-trigger="hover" data-placement="right" data-content="{$r_lang['txt_34']}">?</button></span>
				</div><!-- /input-group -->
			</div>
			<div class="row box-section">
				<div class="input-group">
					<input type="text" name="banned_id" class="form-control" placeholder="{$r_lang['txt_35']}">
					<span class="input-group-btn"><button class="btn btn-warning" type="button" data-rel="popover" data-trigger="hover" data-placement="right" data-content="{$r_lang['txt_36']}">?</button></span>
				</div><!-- /input-group -->
			</div>
			<br><button class="btn btn-default" type="submit" name="submit">{$r_lang['txt_24']}</button>
			<input type="hidden" name="mod" value="rank">
			<input type="hidden" name="action" value="addrank">
		</form>
      </div>
    </div>
  </div>
</div>
HTML;
?>