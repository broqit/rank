<?php
/**
 * Rank
 * Last Updated: $Date: 2016-10-08T21:59:59+03:00 $
 *
 * @version 1.3
 * @author Roman Borkunov [icq: 420159], [skype: coderlaba.com], [email: ya.qnut@ya.ru]
 * @package engine/mods/rank/backend/index.php
 * @license http://coderiz.pw/license [Rank]
 * @copyright (c) 2014 - 2016
 * @link http://coderiz.pw
 *
 */

if( !defined( 'DATALIFEENGINE' ) OR !defined( 'LOGGED_IN' ) ) die( "Hacking attempt!" );

if (file_exists(ROOT_DIR . '/language/' . $selected_language . '/rank.lng')) require_once (ROOT_DIR . '/language/' . $selected_language . '/rank.lng');
else die("Language file not found");

if ($member_id['user_group']!= 1) msg($lang['opt_denied'],$lang['opt_denied']);
$action = totranslit( $_REQUEST['action'] );

require_once ENGINE_DIR . '/mods/rank/backend/functions.php';
require_once ENGINE_DIR . '/data/rank_config.php';

bqheader();
if(!$rank_config['status']) msg_err($r_lang['warn'],$r_lang['off']);

switch ($action) {
	case "options" :
		echo <<<HTML
<form method="post" action="">
    <h1 class="page-header nopad">{$r_lang['txt_8']}</h1>
<div class="panel panel-default">
HTML;
opentable();
showRow( $r_lang['txt_9'], $r_lang['txt_10'], makeCheckBox( "save_con[status]", "{$rank_config['status']}" ) );
showRow( $r_lang['txt_11'], $r_lang['txt_12'], makeDropDown( array( "" => $r_lang['txt_44'], "news_num" => "news_num", "comm_num" => "comm_num ", "rating" => "rating" ), "save_con[section]", $rank_config['section'] ) );
showRow( $r_lang['txt_13'], $r_lang['txt_14'], "<input type=\"text\" class=\"form-control\" name=\"save_con[section_txt]\" value=\"{$rank_config['section_txt']}\">" );
closetable();
echo <<<HTML
</div>
<hr>
<button type="submit" name="submit" class="btn btn-lg btn-success">{$lang['user_save']}</button>
<input type="hidden" name="action" value="save" />
</form>
HTML;
		break;

	case "save" :
		$case = 'save';
		require_once ENGINE_DIR . '/mods/rank/backend/action.php';
		break;

	case "list" :
		require_once ENGINE_DIR . '/mods/rank/backend/list.php';
		break;

	case "addrank" :
		$case = 'add';
		require_once ENGINE_DIR . '/mods/rank/backend/action.php';
		break;

	case "removerank" :
		$case = 'remove';
		require_once ENGINE_DIR . '/mods/rank/backend/action.php';
		break;

	case "editrank" :
		if(isset($_REQUEST['rankid'])){
        $rankid = intval( $_REQUEST['rankid'] );
        $row = $db->super_query( "SELECT * FROM " . PREFIX . "_rank WHERE id = '$rankid'" );
        $row['name'] = stripslashes( preg_replace( array ("'\"'", "'\''" ), array ("&quot;", "&#039;" ), $row['name'] ) );
        $row['descr'] = stripslashes( preg_replace( array ("'\"'", "'\''" ), array ("&quot;", "&#039;" ), $row['descr'] ) );
    echo <<<HTML
<form method="post" action="" class="form-horizontal">
  <input type="hidden" name="action" value="doeditrank">
  <input type="hidden" name="rankid" value="{$rankid}">
  <h1 class="page-header nopad">{$r_lang['txt_41']} (ID: {$rankid})</h1>
<div class="panel panel-default">
  <div class="panel-body">
    <div class="row box-section">
                <div class="input-group">
                    <input type="text" name="name" class="form-control" value="{$row['name']}" placeholder="{$r_lang['txt_27']}">
                    <span class="input-group-btn"><button class="btn btn-warning" type="button" data-rel="popover" data-trigger="hover" data-placement="right" data-content="{$r_lang['txt_28']}">?</button></span>
                </div><!-- /input-group -->
            </div>
            <div class="row box-section">
                <div class="input-group">
                    <input type="text" name="descr" class="form-control" value="{$row['descr']}" placeholder="{$r_lang['txt_29']}">
                    <span class="input-group-btn"><button class="btn btn-warning" type="button" data-rel="popover" data-trigger="hover" data-placement="right" data-content="{$r_lang['txt_30']}">?</button></span>
                </div><!-- /input-group -->
            </div>
            <div class="row box-section">
                <div class="input-group">
                    <input type="text" name="count" class="form-control" value="{$row['count']}" placeholder="{$r_lang['txt_31']}">
                    <span class="input-group-btn"><button class="btn btn-warning" type="button" data-rel="popover" data-trigger="hover" data-placement="right" data-content="{$r_lang['txt_32']}">?</button></span>
                </div><!-- /input-group -->
            </div>
            <div class="row box-section">
                <div class="input-group">
                    <input type="text" name="user_id" class="form-control" value="{$row['user_id']}" placeholder="{$r_lang['txt_33']}">
                    <span class="input-group-btn"><button class="btn btn-warning" type="button" data-rel="popover" data-trigger="hover" data-placement="right" data-content="{$r_lang['txt_34']}">?</button></span>
                </div><!-- /input-group -->
            </div>
            <div class="row box-section">
                <div class="input-group">
                    <input type="text" name="banned_id" class="form-control" value="{$row['banned_id']}" placeholder="{$r_lang['txt_35']}">
                    <span class="input-group-btn"><button class="btn btn-warning" type="button" data-rel="popover" data-trigger="hover" data-placement="right" data-content="{$r_lang['txt_36']}">?</button></span>
                </div><!-- /input-group -->
            </div>
            <br><button class="btn btn-success" type="submit" name="submit">{$r_lang['txt_42']}</button>
   </div>
</div>
</form>
HTML;
} else $stop = alert_err($r_lang['txt_51'], $r_lang['txt_40'],'1');
		break;

	case "doeditrank" :
		$case = 'edit';
		require_once ENGINE_DIR . '/mods/rank/backend/action.php';
		break;

	default:
		$row = $db->super_query("SELECT COUNT(*) as counts FROM " . PREFIX . "_rank");
$status = ($rank_config['status']) ? "<span class=\"label label-success\">{$r_lang['txt_1']}</span>" : "<span class=\"label label-danger\">{$r_lang['txt_2']}</span>";
echo <<<HTML
<script language="javascript" type="text/javascript">
<!--
function check_updates(module,version){
	ShowLoading('');
	$.post("engine/mods/rank/ajax/updates.php", {module_id:module,version_id:version},
    function(data){
		$("#dlepopup").remove();
		$("body").append("<div id='dlepopup' title='{$r_lang['rank_update']}' style='display:none'><br />"+ data +"</div>");
		$('#dlepopup').dialog({
			autoOpen: true,
			width: 500,
			minHeight: 160,
			resizable: false
		});
		HideLoading('');
    });
	return false;
}
//-->
</script>
	<div class="bs-example">
		<div class="row box-section">
			<div class="col-md-10">{$r_lang['txt_3']}</div>
			<div class="col-md-2">{$status}</div>
		</div>
		<div class="row box-section">
			<div class="col-md-10">{$r_lang['txt_4']}</div>
			<div class="col-md-2"><span class="label label-info">{$rank_config['version']}</span></div>
		</div>
		<div class="row box-section">
			<div class="col-md-10">{$r_lang['txt_5']}</div>
			<div class="col-md-2"><span class="label label-default">{$row['counts']}</span></div>
		</div>
	</div>
	<div class="highligh">
		<div class="pull-left" style="line-height:32px;"><b>{$r_lang['txt_6']}</b> <a href="http://coderiz.pw/" target="_blank">Roman Borkunov</a></div>
		<div class="pull-right"><button onclick="check_updates('5','{$rank_config['version']}'); return false;" class="btn btn-default"><i class="icon-exclamation-sign"></i> {$r_lang['txt_7']}</button></div>
		<div style="clear:both;"></div>
	</div>
HTML;
		break;
}

$menu .= menu('main', 'rank', 'dashboard', $r_lang['txt_48']);
$menu .= menu('list', 'rank&action=list', 'th-list', $r_lang['txt_49']);
$menu .= menu('options', 'rank&action=options', 'cog', $r_lang['txt_50']);

echo '</div><div class="col-md-3"><div class="bs-docs-sidebar hidden-print hidden-xs hidden-sm" role="complementary"><div class="list-group">'.$menu.'</div></div></div>';
bqfooter();
?>