<?php
/**
 * Rank
 * Last Updated: $Date: 2016-10-08T22:02:29+03:00 $
 *
 * @version 1.3
 * @author Roman Borkunov [icq: 420159], [skype: coderlaba.com], [email: ya.qnut@ya.ru]
 * @package engine/mods/rank/backend/action.php
 * @license http://coderiz.pw/license [Rank]
 * @copyright (c) 2014 - 2016
 * @link http://coderiz.pw
 *
 */

if( !defined( 'DATALIFEENGINE' ) OR !defined( 'LOGGED_IN' ) ) die( "Hacking attempt!" );

switch ($case) {
    case 'add':
        $name  = $db->safesql(  htmlspecialchars( strip_tags( stripslashes($_POST['name'] ) ), ENT_QUOTES, $config['charset']) );
        $descr  = $db->safesql(  htmlspecialchars( strip_tags( stripslashes($_POST['descr'] ) ), ENT_QUOTES, $config['charset']) );
        $count  = $db->safesql(  htmlspecialchars( strip_tags( stripslashes($_POST['count'] ) ), ENT_QUOTES, $config['charset']) );
        $user_id  = $db->safesql(  htmlspecialchars( strip_tags( stripslashes($_POST['user_id'] ) ), ENT_QUOTES, $config['charset']) );
        $banned_id  = $db->safesql(  htmlspecialchars( strip_tags( stripslashes($_POST['banned_id'] ) ), ENT_QUOTES, $config['charset']) );

        if( intval(5) AND dle_strlen( str_replace(" ", "", strip_tags(trim($name))), $config['charset'] ) < 5 ) {
            $stop = alert_err($r_lang['txt_51'], $r_lang['txt_38'],'1');
            $CN_HALT = TRUE;
        }
        if( !$name ) {
            $stop = alert_err($r_lang['txt_51'], $r_lang['txt_37'],'1');
            $CN_HALT = TRUE;
        }
        if(!$CN_HALT) {
            $db->query( "INSERT INTO " . PREFIX . "_rank (`name`, `descr`, `count`, `user_id`, `banned_id`) VALUES ('$name', '$descr', '$count', '$user_id', '$banned_id')" );
            $db->free();
            $stop = alert_suc($r_lang['txt_54'], $r_lang['txt_39'],'1');
        }
        echo $stop;
        break;

    case 'edit':
        if(isset($_REQUEST['rankid'])){
            $rankid = intval( $_POST['rankid'] );
            $name  = $db->safesql(  htmlspecialchars( strip_tags( stripslashes($_POST['name'] ) ), ENT_QUOTES, $config['charset']) );
            $descr  = $db->safesql(  htmlspecialchars( strip_tags( stripslashes($_POST['descr'] ) ), ENT_QUOTES, $config['charset']) );
            $count  = $db->safesql(  htmlspecialchars( strip_tags( stripslashes($_POST['count'] ) ), ENT_QUOTES, $config['charset']) );
            $user_id  = $db->safesql(  htmlspecialchars( strip_tags( stripslashes($_POST['user_id'] ) ), ENT_QUOTES, $config['charset']) );
            $banned_id  = $db->safesql(  htmlspecialchars( strip_tags( stripslashes($_POST['banned_id'] ) ), ENT_QUOTES, $config['charset']) );

            if( !$rankid ) {
                $stop = alert_err($r_lang['txt_51'], $r_lang['txt_40'],'1');
                $CN_HALT = TRUE;
            }
            $row = $db->super_query( "SELECT id FROM " . PREFIX . "_rank WHERE id = '$rankid'" );

            if( $name == "" ) {
                $stop = alert_err($r_lang['txt_51'], $r_lang['txt_37'],'1');
                $CN_HALT = TRUE;
            }
            if( $row['id'] != $rankid ) {
                $stop = alert_err($r_lang['txt_51'], $r_lang['txt_40'],'1');
                $CN_HALT = TRUE;
            }
            if(!$CN_HALT) {
                $db->query( "UPDATE " . PREFIX . "_rank SET name='$name', descr='$descr', count='$count', user_id='$user_id', banned_id='$banned_id' WHERE id='$rankid'" );
                $stop = alert_suc($r_lang['txt_54'], $r_lang['txt_43'],'1');
            }
        } else $stop = alert_err($r_lang['txt_51'], $r_lang['txt_40'],'1');
        break;

    case 'remove':
        if(isset($_REQUEST['rankid'])) {
            $rankid = intval($_REQUEST['rankid']);
            $checkid = $db->query("SELECT * FROM ".PREFIX."_rank WHERE id='$rankid'");
            if($db->num_rows($ckeckid)>0){
                $remove = $db->query( "DELETE FROM " . PREFIX . "_rank WHERE id = '$rankid'" );
                $stop = ($remove) ? alert_suc($r_lang['txt_73'], $r_lang['txt_45'],'1') : alert_err($r_lang['txt_51'], $r_lang['txt_46'],'1');
            } else $stop = alert_err($r_lang['txt_51'], $r_lang['txt_40'],'1');
        } else $stop = alert_err($r_lang['txt_51'], $r_lang['txt_47'],'1');
        echo $stop;
        break;

    case 'save':
        $save_con = (!empty($_POST['save_con'])) ? $_POST['save_con'] : $rank_config;
        $handler = fopen( ENGINE_DIR.'/data/rank_config.php', "w" );
        fwrite($handler, "<?php\n\$rank_config = array (\n  'version' => \"{$rank_config['version']}\",\n");
        foreach( $save_con as $name => $value ) {
            if( is_array( $value ) ) {
                foreach( $value as $key => $explode ) {
                    $explode = CharsetConvert( $explode );
                    $explode = totranslit( $explode );
                    $value[ $key ] = $explode;
                }
                $value = ( count( $value ) > 0 ) ? implode( ",", $value ) : "";
            } else {
                $value = convert_unicode( $value, $config['charset'] );
                $value = stripslashes( $value );
            }
            fwrite( $handler, " '{$name}' => \"{$value}\",\n" );
        }
        fwrite( $handler, ");\n?>" );
        fclose( $handler );
        if($_POST['save_con']) alert_suc($r_lang['txt_54'],$r_lang['txt_53'],'1');
        else alert_err($r_lang['txt_51'],$r_lang['txt_52'],'1');
        break;

    default:
        alert_err($r_lang['txt_51'], $r_lang['txt_40'],'1');
        break;
}
?>