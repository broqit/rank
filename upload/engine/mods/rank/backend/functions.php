<?php
/**
 * Rank
 * Last Updated: $Date: 2016-03-27T02:46:12+02:00 $
 *
 * @version 1.3
 * @author Roman Borkunov [icq: 420159], [skype: coderlaba.com], [email: ya.qnut@ya.ru]
 * @package engine/mods/rank/backend/functions.php
 * @license http://coderiz.pw/license [Rank]
 * @copyright (c) 2014 - 2016
 * @link http://coderiz.pw
 *
 */

if( !defined( "DATALIFEENGINE" ) ) die( "Hacking attempt!" );

function pagination($module,$action,$start_from,$per_page,$result_count) {
  if( $start_from > 0 ) {
    $previous = $start_from - $per_page;
    $npp_nav .= "<li><a href=\"$PHP_SELF?mod={$module}&action={$action}&start_from={$previous}\" title=\"{$lang['edit_prev']}\">&lt;&lt;</a></li>";
  }

  if( $result_count > $per_page ) {
    $enpages_count = @ceil( $result_count / $per_page );
    $enpages_start_from = 0;
    $enpages = "";

    if( $enpages_count <= 10 ) {
      for($j = 1; $j <= $enpages_count; $j ++) {
        if( $enpages_start_from != $start_from ) $enpages .= "<li><a href=\"$PHP_SELF?mod={$module}&action={$action}&start_from={$enpages_start_from}\">$j</a></li>";
        else $enpages .= "<li class=\"active\"><span>$j</span></li>";

        $enpages_start_from += $per_page;
      }
      $npp_nav .= $enpages;
    } else {
      $start = 1;
      $end = 10;

      if( $start_from > 0 ) {
        if( ($start_from / $per_page) > 4 ) {
          $start = @ceil( $start_from / $per_page ) - 3;
          $end = $start + 9;

          if( $end > $enpages_count ) {
            $start = $enpages_count - 10;
            $end = $enpages_count - 1;
          }
          $enpages_start_from = ($start - 1) * $per_page;
        }
      }
      if( $start > 2 ) $enpages .= "<li><a href=\"$PHP_SELF?mod={$module}&action={$action}&start_from=0\">1</a></li> <li><span>...</span></li>";

      for($j = $start; $j <= $end; $j ++) {
        if( $enpages_start_from != $start_from ) $enpages .= "<li><a href=\"$PHP_SELF?mod={$module}&action={$action}&start_from={$enpages_start_from}\">$j</a></li>";
        else $enpages .= "<li class=\"active\"><span>$j</span></li>";

        $enpages_start_from += $per_page;
      }
      $enpages_start_from = ($enpages_count - 1) * $per_page;
      $enpages .= "<li><span>...</span></li><li><a href=\"$PHP_SELF?mod={$module}&action={$action}&start_from={$enpages_start_from}\">$enpages_count</a></li>";
      $npp_nav .= $enpages;
    }
    if( $result_count > $i ) {
      $how_next = $result_count - $i;
      if( $how_next > $per_page ) $how_next = $per_page;

      $npp_nav .= "<li><a href=\"$PHP_SELF?mod={$module}&action={$action}&start_from={$i}\" title=\"{$lang['edit_next']}\">&gt;&gt;</a></li>";
    }
    $npp_nav = "<div class=\"row box-section text-center\"><ul class=\"pagination pagination-sm\">".$npp_nav."</ul></div>";
  }
  return $npp_nav;
}

function menu($section,$link,$ico,$name) {
  global $action;
  $class = ($action == $section) ? "active" : "";
  return "<a href=\"{$PHP_SELF}?mod={$link}\" class=\"list-group-item {$class}\"><span class=\"badge\"><i class=\"glyphicon glyphicon-{$ico}\"></i></span> {$name}</a>";
}

function bqheader($module = 'rank') {
  global $config, $rank_config, $member_id, $user_group, $lang, $r_lang;

  if($config['version_id'] >= "10.5") {
    if ( count(explode("@", $member_id['foto'])) == 2 ) $avatar = '//www.gravatar.com/avatar/' . md5(trim($member_id['foto'])) . '?s=' . intval($user_group[$row['user_group']]['max_foto']);
    else {
      if( $member_id['foto'] ) {
        $avatar = (strpos($row['foto'], "//") === 0) ? "http:".$member_id['foto'] : $member_id['foto'];
        $avatar = @parse_url ( $avatar );

        $avatar = ($avatar['host']) ? $member_id['foto'] : $config['http_home_url'] . "uploads/fotos/" . $member_id['foto'];
      } else $avatar = "engine/skins/images/noavatar.png";
    }
  } else {
    if ( count(explode("@", $member_id['foto'])) == 2 ) $avatar = 'http://www.gravatar.com/avatar/' . md5(trim($member_id['foto'])) . '?s=' . intval($user_group[$row['user_group']]['max_foto']);
    else $avatar = ($member_id['foto']) ? $config['http_home_url'] . "uploads/fotos/" . $member_id['foto'] : "engine/skins/images/noavatar.png";
  }

  $pop_notice = ( $member_id['pm_unread'] ) ? "<span class=\"badge badge-danger\">{$member_id['pm_unread']}</span>" : "";
  $message_notice = ( $member_id['pm_all'] ) ? "<span class=\"label label-danger pull-right\">{$member_id['pm_all']}</span>" : "";
  $profile_link = $config['http_home_url'] . "user/" . urlencode ( $member_id['name'] ) . "/";

echo <<<HTML
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="{$config['charset']}">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="{$r_lang['descname']}" />
	<meta name="author" content="Roman Borkunov - http://coderiz.pw/" />
	<title>{$r_lang['name']} / {$r_lang['rusname']} v.{$rank_config['version']}</title>
	<link href="engine/mods/{$module}/backend/assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="engine/mods/{$module}/backend/assets/css/docs.min.css" rel="stylesheet">
	<link rel="icon" href="engine/mods/{$module}/backend/assets/css/{$module}.ico">
	<script src="engine/mods/{$module}/backend/assets/js/application.js"></script>
  </head>
  <body>
  <script language="javascript" type="text/javascript">
<!--
var dle_act_lang   = ["{$lang['p_yes']}", "{$lang['p_no']}", "{$lang['p_enter']}", "{$lang['p_cancel']}", "{$lang['media_upload']}"];
var cal_language   = {en:{months:['{$lang['January']}','{$lang['February']}','{$lang['March']}','{$lang['April']}','{$lang['May']}','{$lang['June']}','{$lang['July']}','{$lang['August']}','{$lang['September']}','{$lang['October']}','{$lang['November']}','{$lang['December']}'],dayOfWeek:["{$langdate['Sun']}", "{$langdate['Mon']}", "{$langdate['Tue']}", "{$langdate['Wed']}", "{$langdate['Thu']}", "{$langdate['Fri']}", "{$langdate['Sat']}"]}};
//-->
</script>
<div id="loading-layer">{$lang['ajax_info']}</div>
<div id="wrapper">
    <a class="sr-only sr-only-focusable" href="#content">Skip to main content</a>
    <header class="navbar navbar-static-top bs-docs-nav" id="top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="{$config['http_home_url']}{$config['admin_path']}" class="navbar-brand">{$lang['tabs_gr_admin']}</a>
    </div>
    <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
      <ul class="nav navbar-nav">
        <li class="active">
          <a href="{$config['http_home_url']}{$config['admin_path']}?mod={$module}">{$r_lang['name']}</a>
        </li>
		<li>
          <a href="{$config['http_home_url']}{$config['admin_path']}?mod=options&amp;action=options" target="_blank">{$lang['header_all']}</a>
        </li>
        <li>
          <a href="{$config['http_home_url']}">{$lang['skin_view']}</a>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
		<li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{$member_id['name']} <i class="caret"></i></a>
            <ul class="dropdown-menu" role="menu">
                <li class="with-image">
                  <div class="avatar">
                    <img src="$avatar" />
					{$pop_notice}
                  </div>
                  <span>{$member_id['name']}</span>
                </li>
				<li class="dropdown-header" style="text-align:center;"><span class="label label-default">{$user_group[$member_id['user_group']]['group_name']}</span></li>
                <li class="divider"></li>
                <li><a href="{$config['http_home_url']}user/{$member_id['name']}/" target="_blank"><i class="icon-user"></i> <span>{$lang['header_profile']}</span></a></li>
                <li><a href="{$config['http_home_url']}{$config['admin_path']}?mod=options&action=personal"><i class="icon-cog"></i> <span>{$lang['header_settings']}</span></a></li>
                <li><a href="{$config['http_home_url']}index.php?do=pm" target="_blank"><i class="icon-envelope"></i> <span>{$lang['header_messages']}</span> {$message_notice}</a></li>
            </ul>
        </li>
        <li><a href="{$config['http_home_url']}{$config['admin_path']}?action=logout">{$lang['skin_logout']}</a></li>
      </ul>
    </nav>
  </div>
</header>
    <div class="bs-docs-header" id="content">
      <div class="container">
        <h1>{$r_lang['name']}<span class="label label-success">v.{$rank_config['version']}</span></h1>
        <p>{$r_lang['subname']}</p>
      </div>
    </div>
    <div class="container bs-docs-container">
      <div class="row">
		<div class="col-md-9" role="main">
HTML;
}

function bqfooter() {
	global $rank_config, $r_lang;
	$year = date('Y');
echo <<<HTML
</div>
    </div>
<footer class="bs-docs-footer" role="contentinfo">
  <div class="container">
    <ul class="bs-docs-footer-links muted">
      <li>{$r_lang['name']} v.{$rank_config['version']}</li>
	   <li>Copyright � 2014 - {$year}</li>
	   <li>&middot;</li>
      <li><a href="http://coderiz.pw" target="_blank">{$r_lang['support']}</a></li>
    </ul>
  </div>
</footer>
</div>
  </body>
</html>
HTML;
}
function opentable() {
	echo "<table class=\"table table-striped\">";
}
function closetable() {
	echo "</table>";
}
function showRow($title = "", $description = "", $field = "") {
	echo "<tr>
        <td class=\"col-xs-10 col-sm-6 col-md-7\"><h6>{$title}</h6><span class=\"note large\">{$description}</span></td>
        <td class=\"col-xs-2 col-md-5 settingstd\">{$field}</td>
        </tr>";
}
function makeDropDown($options, $name, $selected) {
	$output = "<select class=\"uniform\" style=\"min-width:100px;\" name=\"$name\">\r\n";
	foreach ( $options as $value => $description ) {
		$output .= "<option value=\"$value\"";
		if( $selected == $value ) $output .= " selected ";
		$output .= ">$description</option>\n";
	}
	$output .= "</select>";
	return $output;
}
function makeCheckBox($name, $selected) {
	$selected = $selected ? "checked" : "";
	return "<input class=\"iButton-icons-tab\" type=\"checkbox\" name=\"$name\" value=\"1\" {$selected}>";
}
function alert_err ($name, $text, $link) {
  global $r_lang;
  $link = ($link) ? '<br><a href="javascript:history.go(-1)">'.$r_lang['txt_210'].'</a>' : "";
  echo "<div class=\"alert alert-danger\"><strong>{$name}</strong><br>{$text}{$link}</div>";
}
function alert_suc ($name, $text, $link) {
  global $r_lang;
  $link = ($link) ? '<br><a href="javascript:history.go(-1)">'.$r_lang['txt_210'].'</a>' : "";
  echo "<div class=\"alert alert-success\"><strong>{$name}</strong><br>{$text}{$link}</div>";
}
function msg_err ($name, $text, $link) {
  global $r_lang;
  $link = ($link) ? '<br><a href="javascript:history.go(-1)">'.$r_lang['txt_210'].'</a>' : "";
  echo "<div class=\"bs-callout bs-callout-danger\"><h4>{$name}</h4><p>{$text}{$link}</p></div>";
}
?>