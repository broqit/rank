<?php
/**
 * Rank
 * Last Updated: $Date: 08.10.2016 21:59:19  $
 *
 * @version 1.3
 * @author Roman Borkunov [icq: 420159], [skype: coderlaba.com], [email: ya.qnut@ya.ru]
 * @package engine/inc/rank.php
 * @license http://coderiz.pw/license [Rank]
 * @copyright (c) 2014 - 2016
 * @link http://coderiz.pw
 *
 */

require ENGINE_DIR . "/mods/rank/backend/index.php";
?>