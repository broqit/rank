# Rank
- author: Roman Borkunov <broqit>
- version DLE: 10.2 - 12.0
- website: https://coderiz.pw

Copyright (c) 2017 Copyright Holder All Rights Reserved.

# Install

1. Copy all files from **upload** folder to your server.
2. Run **install.php**
3. Open **engine/ajax/profile.php**, find:
```
$tpl->set( '{lastdate}', langdate( "j F Y H:i", $row['lastdate'] ) );
```
Ниже вставляем:
```
require_once ENGINE_DIR.'/mods/rank/frontend/rank.php';
```
4. Открываем **engine/modules/profile.php** в нем ищем:
```
$tpl->set( '{usertitle}', stripslashes( $row['name'] ) );
```
Ниже вставляем:
```
require_once ENGINE_DIR.'/mods/rank/frontend/rank.php';
```
5. Открываем **engine/classes/comments.class.php** в нем ищем:
```
$tpl->set( '{id}', $row['id'] );
```
Ниже вставляем:
```
require ENGINE_DIR.'/mods/rank/frontend/rank.php';
```
6. Открываем **templates/{THEME}/userinfo.tpl**, **templates/{THEME}/profile_popup.tpl**, **templates/{THEME}/comments.tpl** в нужном месте вставляем:
```
[rank]Ранг: {rank}[/rank]
{rank_id} - ID - звания
{rank_descr} - описание звания
{next_rank} - кол-во очков до след. звания
{rank_img} - ссылка на изображение звания, которое должно хранится в папке шаблона
rank/img/1.png, rank/img/2.png и т.п.( название изображения соответствует id звания и обязательно имеет формат .png)
{next_name} - выводит имя следующего звания
```
